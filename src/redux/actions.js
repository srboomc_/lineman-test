import {getDistrict} from '../services/District';

export const GET_DISTRICT = 'GET_DISTINCT';
export const GET_DISTRICT_FAIL = 'GET_DISTINCT_FAIL';

export const getDistrictAction = () => {
  return async dispatch => {
    try {
      const data = await getDistrict();
      if (data) {
        dispatch({
          type: GET_DISTRICT,
          payload: data,
        });
      } else {
        dispatch({
          type: GET_DISTRICT_FAIL,
        });
      }
    } catch (error) {
      dispatch({
        type: GET_DISTRICT_FAIL,
      });
    }
  };
};
