import {GET_DISTRICT, GET_DISTRICT_FAIL} from './actions';

const initialState = {
  district: [],
  isLoading: true,
  isError: false,
};

const districtReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DISTRICT:
      return {
        ...state,
        district: action.payload,
        isLoading: false,
        isError: false,
      };
    case GET_DISTRICT_FAIL:
      return {
        ...state,
        district: action.payload,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};
export default districtReducer;
