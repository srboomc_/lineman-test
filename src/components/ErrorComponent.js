import React from 'react';
import {View, StyleSheet, Text, Button} from 'react-native';

const ErrorComponent = props => {
  return (
    <View style={styles.container}>
      <Text>Opps, something went wrong</Text>
      <Button title="Try Again" onPress={props.onPress} />
    </View>
  );
};
export default ErrorComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
