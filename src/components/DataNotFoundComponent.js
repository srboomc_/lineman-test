import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const DataNotFoundComponent = () => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.title}>Sorry, data not found.</Text>
      </View>
    </>
  );
};
export default DataNotFoundComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
    fontFamily: 'Kanit-Medium',
    color: '#000',
  },
});
