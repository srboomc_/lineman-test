import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';

const SearchbarComponent = props => {
  return (
    <View style={styles.container}>
      <TextInput
        placeholder="Search"
        style={styles.inputStyle}
        autoCorrect={false}
        clearButtonMode="always"
        onChangeText={props.onChange}
        value={props.value}
      />
    </View>
  );
};

export default SearchbarComponent;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    height: 45,
    borderRadius: 5,
    flexDirection: 'row',
    margin: 5,
    borderWidth: 1,
    borderColor: '#CCC',
  },
  inputStyle: {
    flex: 1,
    fontSize: 16,
  },
});
