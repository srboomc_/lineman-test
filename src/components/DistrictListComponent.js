import React, {memo} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const DistrictListComponent = props => {
  return (
    <>
      <View style={styles.container}>
        <Text style={styles.title}>
          {props.subdistrict} {props.district}
        </Text>
        <Text style={styles.subtitle}>{props.province}</Text>
      </View>
    </>
  );
};

export default memo(DistrictListComponent);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  title: {
    fontSize: 18,
    color: '#000',
    fontFamily: 'Kanit-Medium',
  },
  subtitle: {
    fontSize: 14,
    fontFamily: 'Kanit-Medium',
    color: '#000',
  },
});
