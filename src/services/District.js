import axios from 'axios';
import {API_URL} from '@env';

export const getDistrict = async () => {
  const response = await axios.get(`${API_URL}`);
  return response.data;
};
