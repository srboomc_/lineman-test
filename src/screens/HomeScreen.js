import React, {useCallback, useEffect, useState} from 'react';
import {View, StyleSheet, FlatList, RefreshControl} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import DistrictListComponent from '../components/DistrictListComponent';
import LoadingComponent from '../components/LoadingComponent';
import ErrorComponent from '../components/ErrorComponent';
import SearchbarComponent from '../components/SearchbarComponent';
import {getDistrictAction} from '../redux/actions';
import DataNotFoundComponent from '../components/DataNotFoundComponent';

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const HomeScreen = props => {
  const districtState = useSelector(state => state.districtReducer);
  const dispatch = useDispatch();
  const [refreshing, setRefreshing] = useState(false);
  const [search, setSearch] = useState('');
  const [filteredDistrict, setFilteredDistrict] = useState([]);
  const [isEmpty, setIsEmpty] = useState(false);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    wait(1000).then(() => {
      setRefreshing(false);
      setSearch('');
      dispatch(getDistrictAction());
    });
  }, [dispatch]);

  useEffect(() => {
    if (districtState.isLoading) {
      dispatch(getDistrictAction());
    } else {
      setFilteredDistrict(districtState.district);
    }
  }, [dispatch, districtState.isLoading, districtState.district]);

  const onTextChange = text => {
    setSearch(text);
    const newDistrict = districtState.district.filter(
      item =>
        item.subdistrict.toLowerCase().includes(text.toLowerCase()) ||
        item.district.toLowerCase().includes(text.toLowerCase()) ||
        item.province.toLowerCase().includes(text.toLowerCase()),
    );
    setFilteredDistrict(newDistrict);

    if(newDistrict.length === 0){
      setIsEmpty(true);
    }else{
      setIsEmpty(false);
    }
  };

  if (districtState.isLoading || refreshing) {
    return <LoadingComponent />;
  } else if (districtState.isError) {
    return <ErrorComponent onPress={onRefresh} />;
  }

  return (
    <>
      <View style={styles.searchContainer}>
        <SearchbarComponent
          search={search}
          value={search}
          onChange={newSearch => onTextChange(newSearch)}
        />
      </View>
      { isEmpty && <DataNotFoundComponent />}
      <FlatList
        data={filteredDistrict}
        initialNumToRender={10}
        renderItem={({item}) => <DistrictListComponent {...item} />}
        keyExtractor={(item, key) => key.toString()}
        ItemSeparatorComponent={() => <View style={styles.line} />}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      />
    </>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  searchContainer: {
    padding: 10,
  },
  line: {
    width: '100%',
    borderBottomColor: 'lightgrey',
    borderBottomWidth: 1,
  },
});
